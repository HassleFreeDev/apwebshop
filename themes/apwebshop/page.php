<!-- Include header.php -->
<?php get_header(); ?>

<div class="container">
  
  <?php the_content(); ?>

</div>

<!-- Include footer.php -->
<?php get_footer(); ?>
<!-- Include header.php -->
<?php get_header(); ?>

<div class="container">
    <section class="kontakt-container">
        
        <img src="<?php the_field('image-kontakt'); ?>" alt="<?php get_post_meta(the_field('image-kontakt'), '_wp_attachment_image_alt', true); ?>">
        
        <div class="kontakt-info-box">
            <div class="heading-container">
                <h5><?php the_field('small-heading-container'); ?><!-- Wer steckt hinter APWebshop? --></h5>
            </div>
            <p class="heading-info-box-kontakt"><?php the_field('heading-container'); ?><!-- Über Andreas Palitzschs Onlineshop --></p>

            <?php the_content(); ?>
            
        </div>
    </section>

</div>
<!-- Include footer.php -->
<?php get_footer(); ?>
/* Use jquery in wp strict mode */
jQuery(document).ready(function ($) {

    /* Initialize slick slider and get class / wrapper */
    jQuery('.product-slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 2,
    });

    /* Initialize slick slider and get class / wrapper */
    /* Show first slider thumbnail in main image */
    jQuery('.single-slider')
        .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            /* Get actual slick-index */
            var nextSlideElement = $(this).find('[data-slick-index="' + nextSlide + '"]');
            /* Get firts div of image wrapper (main image) */
            var imageContainer = $('.woocommerce-product-gallery__wrapper > div').first();
            /* Replace main image with first thumbnail in slider */
            changeMainImage(nextSlideElement, imageContainer);
        })
        .slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
        });

    /* single-product.php single slider - click on thumbnail in slider and show in main image */
    function changeMainImage(activeSlide, imageContainer) {
        let imgMax = activeSlide.find('img');
        console.log(imgMax.attr('width'));

        /* Get data-thumb of clicked element (thumbnail in slider)*/
        var dataThumb = activeSlide.data('thumb');
        /* Get href of clicked element (thumbnail in slider) */
        var href = activeSlide.find('a').first().attr('href');
        /* Get <img of clicked element and clone it completely*/
        var img = activeSlide.find('img').clone();

        /* console.log(img); */

        /* Replace main image data-thumb with data-thumb of clicked element */
        imageContainer.data('thumb', dataThumb);
        /* Replace main image href with href of clicked element */
        imageContainer.find('a').first().attr('href', href);
        /* Replace img inside a with img of clicked element (thumbnail) */
        imageContainer.find('a img').replaceWith(img);
        /* Remove current .zoomImg img */
        imageContainer.find('.zoomImg').remove();
        /* If desktop width is less than, do... (prevent on bigger screens, that zoom get initialized and the zoom function uses a smaller image than the main image to 'zoom') */
        if ($(window).width() < 1900) {
            /* Reinitialize zoom function for new image*/
            imageContainer.zoom();
        }
    }

    $('.slick-slide').click(function () {
        /* Set imageContainer -> first div */
        var imageContainer = $('.woocommerce-product-gallery__wrapper > div').first();
        /* Get clicked slide (this) and replace main image with thumbnail */
        changeMainImage($(this), imageContainer);
    });

    /* Enable smooth scroll on all links */
    $(document).ready(function () {
        // Add smooth scrolling to all links
        $("a").on('click', function (e) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                e.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (600) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 700, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });

    /* Change backgroundcolor from transparent to visible color - header.php (Main navigation - only for header with hero image) */
    $(function () {
        var containerHeroNavigation = $('.container-hero-navigation');
        var containerHeroNavigationElse = $('.container-hero-navigation.else');

        $(window).scroll(function () {
            var scroll = $(window).scrollTop();


            if (scroll >= 100) {
                containerHeroNavigation.css('background-color', '#9A9697');
                containerHeroNavigation.css('position', 'fixed');
                containerHeroNavigation.css('z-index', '100');
                containerHeroNavigation.css('transition', 'all .5s ease-in-out');
                containerHeroNavigation.css('border-bottom', '1px solid #007a9e');

            } else if (scroll == 0) {
                containerHeroNavigation.css('background-color', 'transparent');
                containerHeroNavigation.css('transition', 'all 1s ease-in-out');
                containerHeroNavigation.css('border-bottom', '1px solid transparent');
            }
        });
    });
});
<!DOCTYPE html>
<html <?php language_attributes(); ?>> <!-- Get the set/used language e.g. de-DE -->
<head>
  <meta charset="<?php bloginfo('charset'); ?>"> <!-- get the set/used charset - in this case UTF-8 -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">

  <!-- Display site name, set in the backend, in the tab bar -->
  <title><?php bloginfo('name'); ?></title>

  <!-- Insert all needed head tags - !important -->
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>> <!-- let add classes dynamically to the body '(e.g. woocommerce)', show admin bar in frontent when looged in -->

<header>
    <!-- Display hero image from wp custom-header -->
    <?php 
    
    if(is_shop()) { ?>

        <div class="container-hero-is-shop">
            <img alt="" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>">
            
            <div class="container-hero-navigation">

                <a href="<?php echo home_url(); ?>"> <!-- Link to home page -->
                    <div class="logo"><?php bloginfo('name'); ?></div>
                </a> 
                
                <nav class="hero-navigation">
                    <div class="search">
                        <?php get_product_search_form(); ?>
                    </div>

                    <div class="top-menu">
                        <?php 
                            if(is_user_logged_in()) { ?>
                                <!-- Add registered menu to the site -->
                                <?php wp_nav_menu(['theme_location' => 'is-logged-in-menu']); ?>
                            <?php } else { ?>
                                <!-- Add registered menu to the site -->
                                <?php wp_nav_menu(['theme_location' => 'hero-menu']); ?>
                            <?php } ?>
                    </div>
                </nav>
            </div>
        </div>
    
    <?php } elseif (is_page('ueber-uns')) { ?>

    <div class="container-hero-is-shop">
        <img src="<?php the_field('hero-image'); ?>" alt="<?php get_post_meta(the_field('hero-image'), '_wp_attachment_image_alt', true); ?>">

        <div class="container-hero-navigation">
            <a href="<?php echo home_url(); ?>"> <!-- Link to home page -->
                <div class="logo"><?php bloginfo('name'); ?></div>
            </a> 
            
            <nav class="hero-navigation">
                <div class="search">
                    <?php get_product_search_form(); ?>
                </div>

                <div class="top-menu">
                    <?php 
                        if(is_user_logged_in()) { ?>
                            <!-- Add registered menu to the site -->
                            <?php wp_nav_menu(['theme_location' => 'is-logged-in-menu']); ?>
                        <?php } else { ?>
                            <!-- Add registered menu to the site -->
                            <?php wp_nav_menu(['theme_location' => 'hero-menu']); ?>
                        <?php } ?>
                </div>
            </nav>
        </div>
    </div>


    <?php } else { ?>

        <div class="container-hero">

            <div class="container-hero-navigation-else">
                <a href="<?php echo home_url(); ?>"> <!-- Link to home page -->
                    <div class="logo"><?php bloginfo('name'); ?></div>
                </a> 

            <nav class="hero-navigation">
                <div class="search">
                <?php get_product_search_form(); ?>
                </div>

                <div class="top-menu">
                <!-- Add registered menu to the site -->
                <?php 
                    if(is_user_logged_in()) { ?>
                        <!-- Add registered menu to the site -->
                        <?php wp_nav_menu(['theme_location' => 'is-logged-in-menu']); ?>
                    <?php } else { ?>
                        <!-- Add registered menu to the site -->
                        <?php wp_nav_menu(['theme_location' => 'hero-menu']); ?>
                    <?php } ?>
                </div>
            </nav>
            </div>
        </div>
    <?php } ?>

    

  <div class="category-container">
    <nav class="navigation-main">
      <!-- Add registered menu to the site -->
      <?php wp_nav_menu(['theme_location' => 'cat-menu']); ?>
    </nav>
  </div>
</header>
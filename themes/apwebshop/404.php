<!-- Include header.php -->
<?php get_header(); ?>

<div class="container">
    <div class="msg-container">
        <p class="msg">404: Die Seite die du suchst, <span>existiert nicht.</span></p>
    </div>
</div>

<!-- Include footer.php -->
<?php get_footer(); ?>
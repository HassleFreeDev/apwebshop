<footer>
  <div class="footer-container">
      <div class="about-social-container">
        <!-- Add registered menu to the site -->
        <?php wp_nav_menu(['theme_location' => 'footer-left-menu']); ?>
        <i class=" fab fa-instagram"></i>
        <i class=" fab fa-facebook"></i>
      </div>
  
      <div class="impressum-kontakt-container">
        <!-- Add registered menu to the site -->
        <?php wp_nav_menu(['theme_location' => 'footer-right-menu']); ?>
      </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
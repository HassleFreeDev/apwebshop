<?php
/* Remove sidebar from WooCommerce */
  remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar');

/* Unhook WooCommerce wrappers */
  remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
  remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

/* Hook in own wrappers to show content / loop */
  add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 5);
  add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 5);

  function my_theme_wrapper_start() {
    echo '<div class="container">';
  }

  function my_theme_wrapper_end() {
    echo '</div>';
  }

/* Hook in get_header to display header */
add_action('get_header', 'my_theme_header', 20);

function my_theme_header() {
  get_template_part('inc/header');
}

/* Hook in get_footer to display footer */
add_action('get_footer', 'my_theme_footer', 10);

function my_theme_footer() {
  get_template_part('inc/footer');
}

/* Remove page title */
  add_filter('woocommerce_show_page_title', 'toggle_page_title');

  function toggle_page_title($bool) {
    $bool = false;
    return $bool;
  }

/* Remove results */
  remove_action('woocommerce_before_shop_loop','woocommerce_result_count', 20);

/* Remove woocommerce product filter */  
  remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering', 30);

/* Remove Pagination from shop page */
  remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);

  
/* Product order random - Customizer > Woocommerce > Produktkatalog > Standard-Produktsortierung*/
  add_filter( 'woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args' );

  function custom_woocommerce_get_catalog_ordering_args( $args ) {
      $orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

      if ( 'random_list' == $orderby_value ) {
          $args['orderby'] = 'rand';
          $args['order'] = '';
          $args['meta_key'] = '';
      }
      return $args;
  }

  add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_woocommerce_catalog_orderby' );
  add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );

  function custom_woocommerce_catalog_orderby( $sortby ) {
      $sortby['random_list'] = 'Random';
      return $sortby;
  }

/* Heading between shop loops (archive-product.php)*/
  add_action('woocommerce_after_shop_loop', 'top_selling_prod_heading', 30);

  function top_selling_prod_heading() {
    echo '<h2 class="top-products-heading">Beliebte Produkte</h2>';
  }

/* Add gallery functionality - zoom, lightbox and slider (default on single-product.php) */
add_action( 'after_setup_theme', 'wc_gallery_functionality' );
 
  function wc_gallery_functionality() {
      remove_theme_support( 'wc-product-gallery-zoom' );
      remove_theme_support( 'wc-product-gallery-lightbox' );
      remove_theme_support( 'wc-product-gallery-slider' );
  }


/* Change position / layout from default layout */
/* Change priority from 'add to cart' on single-product.php */ /* Need to 'clear:both' on .quantity */
  remove_action('woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30);
  add_action('woocommerce_after_single_product_summary', 'woocommerce_simple_add_to_cart', 5);

/* Remove featured products */
      remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

    /* Remove default position from upsell products */
      remove_action('woocommerce_after_single_product_summary','woocommerce_upsell_display', 15);
      
    /* Add new position (priority) to upsell products*/
      add_action('woocommerce_after_single_product_summary','woocommerce_upsell_display', 6);

/* Add slick slider around thumbnails on 'single-product.php' */
  add_action('woocommerce_product_thumbnails', 'thumb_slider_start', 10);
  add_action('woocommerce_product_thumbnails', 'thumb_slider_end', 30);

  function thumb_slider_start() {
    echo '<div class="single-slider">';
  }

  function thumb_slider_end() {
    echo '</div>';
  }

/* Disable wc-lightbox on click single-product.php (everywhere, where a lightbox occures) */
/* add_filter('woocommerce_single_product_image_thumbnail_html','wc_remove_link_on_thumbnails' );
 
function wc_remove_link_on_thumbnails( $html ) {
     return strip_tags( $html,'<img>' );
} */

/* Change priority of single-product informations (layout purpose) on single-product.php */
/* Price next to main image */ /* Changing the priority throws an issue with germanized ~> choosen variation price doesn't change from range to choosen price */
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 70);

/* Via filter, change price range to price chosen variation - if product is variable */
add_filter( 'woocommerce_gzd_add_to_cart_variation_params', 'my_child_adjust_variation_params', 10 );
function my_child_adjust_variation_params( $params ) {
$params['price_selector'] = 'p.price';

return $params;
}

/* Tax */
remove_action('woocommerce_single_product_summary', 'woocommerce_gzd_template_single_legal_info', 12);
add_action('woocommerce_single_product_summary', 'woocommerce_gzd_template_single_legal_info', 80);

/* Articlenumber & Category */
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 7);

/* Read more button  */
function single_product_read_more_btn() {
  echo '<a href="#tab-title-description" id="read-more-btn" class="read-more-btn">Erfahre mehr</a>';
}

add_action('woocommerce_single_product_summary', 'single_product_read_more_btn', 25);

/* Delivery time Germanized */
remove_action('woocommerce_single_product_summary', 'woocommerce_gzd_template_single_delivery_time_info', 27);
add_action('woocommerce_single_product_summary', 'woocommerce_gzd_template_single_delivery_time_info', 90);

/* Reposition variable product form and variable (product) add to cart (below main image) - single-product.php
 *
 * Change located in: apwebshop/woocommerce/single-product.php
 * Inside the loop ~> Reason: Check if is simple or variable product (only * works inside the loop!!) ~> * - if ($product->is_type('simple')) ~> place add to cart button below image
 * - if ($product->is_type('variable')) ~> remove simpüle add to cart button and replace with variable product form
 *
 */

/* Custom look in woocommerce cart is empty - via hook */
remove_action('woocommerce_cart_is_empty', 'wc_empty_cart_message', 10);
add_action('woocommerce_cart_is_empty', 'custom_woocommerce_cart_is_empty', 10); 

function custom_woocommerce_cart_is_empty() {
    echo '
    <div class="msg-container"><p class="msg">' . __( 'Your cart is currently empty.', 'woocommerce' ) . '</p></div>  
    ';
}
<?php add_action('woocommerce_after_shop_loop', 'best_selling_slider', 40); ?>
<?php function best_selling_slider() { ?>
    
    <div class="top-products-container">
      <?php
        $args = array(
          'post_type'       => 'product',
          'meta_key'        => 'total_sales', /* Show top sales in slider */
          'orderby'         => 'meta_value_num',
          'posts_per_page'  => 12
          );

        $loop = new WP_Query( $args );

        if ( $loop->have_posts() ) { ?>
        
        <div class="product-slider">
          <?php while ( $loop->have_posts() ) : $loop->the_post();
            wc_get_template_part( 'woo-top-sales-content', 'product' );
          endwhile; ?>
        
        </div>
        
        <?php } else {
          echo __( 'No products found' );
        }
        wp_reset_postdata();
      ?>
  </div>
<?php }
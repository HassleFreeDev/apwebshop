<?php
/* Including WooCommerce changes via hooks, etc. */
if(class_exists('woocommerce')) {
    require_once( __DIR__ . '/inc/wc-functions.php');
    require_once( __DIR__ . '/inc/wc-top-selling-slider.php');
}

/* Enqueue stylesheets */
  function load_stylesheets() {
    /* Add fontawesome icons to wp */
     wp_enqueue_style( 'font-awesome', 'https://pro.fontawesome.com/releases/v5.10.0/css/all.css', [], '4.5.0', 'all'); 
    
    /* Add google font to wp */
    wp_enqueue_style('google_fonts', 'https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap', [], '', 'all');

    /* Add slick stylesheet to wp */
    wp_register_style('slick_css', get_template_directory_uri() . '/dist/css/slick.css', [], '', 'all');
    wp_enqueue_style('slick_css');

    /* Add slick-theme stylesheet to wp */
    wp_register_style('slick_theme_css', get_template_directory_uri() . '/dist/css/slick-theme.css', [], '', 'all');
    wp_enqueue_style('slick_theme_css');
    
    /* Add main css stylesheet to wp */
    wp_register_style('main_css', get_template_directory_uri() . '/dist/css/main.css', [], '', 'all');
    wp_enqueue_style('main_css');
  }
  add_action('wp_enqueue_scripts', 'load_stylesheets');


/* Enqueue scripts */
  function load_scripts() {
    /* Add slick js to wp */
    wp_register_script('slick_js', get_template_directory_uri() . '/dist/js/slick.min.js', [], '', true); /* 'true' adds js to the very bottom of the file */
    wp_enqueue_script('slick_js');
    
    /* Add main js to wp */
    wp_register_script('main_js', get_template_directory_uri() . '/dist/js/main.js', [], '', true); /* 'true' adds js to the very bottom of the file */
    wp_enqueue_script('main_js');
    
  }
  add_action('wp_enqueue_scripts', 'load_scripts');


/* Add theme support */
  // Custom Header
  add_theme_support('custom-header');


/* Custom header config */
function apwebshop_custom_header_config() {
    $default = [
      // Default header image to display
      'default-image' => get_template_directory_uri() . '/img/hero-zakaria-zayane-unsplash-edit.jpg',
      'width'         => '2500', /* Default width */
      'height'        => '600',  /* Default height */
      'flex-width'    => true,
      'flex-height'   => true,
    ];
    add_theme_support('custom-header', $default);
  }
  add_action('after_setup_theme', 'apwebshop_custom_header_config');


/* Add custom menus */
  function register_menus() {
    register_nav_menus(array(
        'hero-menu'           => __('Kopf Menü (Oberes Menü neben Suche)'),
        'is-logged-in-menu'   => __('Kopf Menü - Wenn eingeloggt (Oberes Menü neben Suche)'),
        'cat-menu'            => __('Kategorie Menü (Unter Kopfbereich)'),
        'footer-right-menu'   => __('Fußbereich Rechts Menü (Unteres Menü)'),
        'footer-left-menu'    => __('Fußbereich Links Menü (Unteres Menü)')
        )
    ) { array(
        'fallback_cd'         => false, /* If the menu is empty, hide it */
        'container'           => false /* Remove default container from navigation elements */
       ) 
    };
  }
  add_action('after_setup_theme', 'register_menus');


/* // Add classes to !all! custom menus -> 'li' 
  function add_class_to_li($classes, $item, $args) {
    $classes[] = 'custom-li-class another-custom-class';
    return $classes;
  }
  add_filter('nav_menu_css_class', 'add_class_to_li', 1, 3); 
*/

/* // Add classes to !all! custom menus -> 'a' 
  function add_class_to_a($classes, $item, $args) {
    $classes['class'] = 'custom-anchor-class another-custom-anchor-class';
    return $classes;
  }
  add_filter('nav_menu_link_attributes', 'add_class_to_a', 1, 3);
*/

/* Declaring WooCommerce support - with settings */
  function apwebshop_add_woocommerce_support() {
    add_theme_support('woocommerce', array(
      'thumbnail_image_width' => 400,
      'single_image_width'    => 600,

          'product_grid'        => array(
              'default_rows'    => 3,
              'min_rows'        => 1,
              'max_rows'        => 4,
              'default_columns' => 5,
              'min_columns'     => 5,
              'max_columns'     => 5
          ),
    ));

  /* Activate / disable single-product image settings */  
      add_theme_support( 'wc-product-gallery-zoom' );
      add_theme_support( 'wc-product-gallery-lightbox' ); 
      add_theme_support( 'wc-product-gallery-slider' );
  }
  add_action( 'after_setup_theme', 'apwebshop_add_woocommerce_support', 10);
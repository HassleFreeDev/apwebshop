<!-- Include header.php -->
<?php get_header(); ?>

<div class="container">
    <section class="about-us-top-container">
        
        <img src="<?php the_field('about-us-top-image'); ?>" alt="<?php get_post_meta(the_field('about-us-top-image'), '_wp_attachment_image_alt', true); ?>">
        
        <div class="about-us-top-info-box">
            <div class="heading-container">
                <h5><?php the_field('small-heading-top-container'); ?><!-- Wer steckt hinter APWebshop? --></h5>
            </div>
            <h3><?php the_field('heading-top-container'); ?><!-- Über Andreas Palitzschs Onlineshop --></h3>

            <?php the_field('text-top-container'); ?>
            
        </div>
    </section>

    <section class="about-us-middle-container">
        <div class="overlay-container">
            <img src="<?php the_field('about-us-center-image'); ?>" alt="<?php get_post_meta(the_field('about-us-middle-image'), '_wp_attachment_image_alt', true); ?>">
            <div class="image-overlay"></div>
        </div>
        <div class="about-us-middle-absolute-container">
            <div class="heading-middle-container">
                <h5><?php the_field('small-heading-center-container'); ?><!-- Passion und Antrieb --></h5>
                <h3><?php the_field('heading-center-container'); ?><!-- Warum, Wieso, Weshalb --></h3>
            </div>
            <div class="container-paragraph">

                <?php the_field('text-center-container'); ?>

            </div>
        </div>
    </section>

    <section class="about-us-bottom-container">
        <div class="about-us-bottom-info-box">
            <div class="heading-container">
                <h5><?php the_field('small-heading-bottom-container'); ?><!-- Wer steckt hinter APWebshop? --></h5>
            </div>
            <h3><?php the_field('heading-bottom-container'); ?><!-- Über Andreas Palitzschs Onlineshop --></h3>
            
            <?php the_field('text-bottom-container'); ?>
        </div>

        <img src="<?php the_field('about-us-bottom-image'); ?>" alt="<?php get_post_meta(the_field('about-us-bottom-image'), '_wp_attachment_image_alt', true); ?>">
    </section>
</div>
<!-- Include footer.php -->
<?php get_footer(); ?>